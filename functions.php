<?php
/**
 * Mohole functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Mohole
 */

if ( ! function_exists( 'mohole_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function mohole_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Mohole, use a find and replace
		 * to change 'mohole' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'mohole', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			//'menu-1' => esc_html__( 'Primary', 'mohole' ),
			'top_menu' => esc_html__( 'Top menu', 'mohole' ),
			'main_menu' => esc_html__( 'Main menu', 'mohole' ),
			'footer_menu' => esc_html__( 'Footer menu', 'mohole' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'mohole_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'mohole_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function mohole_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'mohole_content_width', 1920 );
}
add_action( 'after_setup_theme', 'mohole_content_width', 0 );

/**
 * Aggiungo un formato immagine personalizzato per la featured image / hero image / slider
 * Dato che il valore di $GLOBALS['content_width'] imposta un limite di larghezza
 * massima per le immagini caricabili è necessario verificare che
 * questo valore sia maggiore o uguale al parametro width di add_image_size()
 */

add_action('after_setup_theme', 'mohole_set_custom_sizes_to_images');
function mohole_set_custom_sizes_to_images() {
	add_image_size('mohole_hero_image', 1920, 600, array ('center', 'center'));
}
add_filter( 'image_size_names_choose', 'mohole_add_custom_sizes_to_images' );
function mohole_add_custom_sizes_to_images( $sizes ) {
    return array_merge( $sizes, array(
        'mohole_hero_image' => __( 'Mohole hero image' ),
    ) );
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mohole_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'mohole' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'mohole' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'mohole_widgets_init' );

/**
 * Impostazioni per il backend
 */
add_action( 'init', function(){
	global $wp_rewrite;
	/* rimuovo i paragrafi automatici */
	//remove_filter( 'the_content', 'wpautop' );
	/* aggiungo i tag alle pagine */
	//register_taxonomy_for_object_type('post_tag', 'page');
	/* aggiungo le categorie alle pagine */
	//register_taxonomy_for_object_type('category', 'page');
	/* cambio lo slug di default per la pagina autore */
	$wp_rewrite->author_base = 'profilo';
});

/**
 * Modifico lo slug di default per le categorie
 * il valore impostato si vede anche nella pagina dei permalink del backend
 * https://wpdreamer.com/2014/01/how-to-change-your-wordpress-category-tag-or-post-format-permalink-structure
 */
add_filter( 'pre_option_category_base', 'mohole_change_category_slug' );
function mohole_change_category_slug( $value ) {
   return 'categoria';
}

/**
* Modifico l'url dell'archivio post di un utente sostituendo lo username con il nickname
* N.B. Ricordarsi di modificare il nickname dell'admin di WP (che di default è uguale allo username)
* https://wordpress.stackexchange.com/questions/5742/change-the-author-slug-from-username-to-nickname#answer-6527
*/
add_filter( 'request', 'wpse5742_request' );
//intercetta la richiesta di un autore
function wpse5742_request( $query_vars )
{
    if ( array_key_exists( 'author_name', $query_vars ) ) {
        global $wpdb;
		$author_id = $wpdb->get_var( $wpdb->prepare( "SELECT user_id FROM {$wpdb->usermeta} WHERE meta_key='nickname' AND meta_value = %s", $query_vars['author_name'] ) );
        if ( $author_id ) {
            $query_vars['author'] = $author_id;
            unset( $query_vars['author_name'] );    
        }
    }
    return $query_vars;
}
add_filter( 'author_link', 'wpse5742_author_link', 10, 3 );
//modifica l'url autore da /author/username in /author/nickname
function wpse5742_author_link( $link, $author_id, $author_nicename )
{
    $author_nickname = get_user_meta( $author_id, 'nickname', true );
    if ($author_nickname) $link = str_replace( $author_nicename, $author_nickname, $link );
    return $link;
}

/**
 * Enqueue scripts and styles.
 */
function mohole_scripts() {
	wp_enqueue_style( 'mohole-style', get_stylesheet_uri() );
	wp_enqueue_style( 'mohole-style-variables', get_template_directory_uri() . '/css/vars.css', array('mohole-style'));
	wp_enqueue_style( 'mohole-style-layout', get_template_directory_uri() . '/css/layout.css', array('mohole-style'));
	wp_enqueue_style( 'mohole-style-header', get_template_directory_uri() . '/css/header.css', array('mohole-style'));
	wp_enqueue_style( 'mohole-style-footer', get_template_directory_uri() . '/css/footer.css', array('mohole-style'));
	wp_enqueue_style( 'mohole-style-sidebar', get_template_directory_uri() . '/css/sidebar.css', array('mohole-style'));
	wp_enqueue_style( 'mohole-style-homepage', get_template_directory_uri() . '/css/homepage.css', array('mohole-style'));
	wp_enqueue_style( 'mohole-style-content', get_template_directory_uri() . '/css/content.css', array('mohole-style'));
	
	//Fonts
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
	wp_enqueue_style( 'roboto', 'https://fonts.googleapis.com/css?family=Roboto');
	wp_enqueue_style( 'montserrat', 'https://fonts.googleapis.com/css?family=Montserrat+Alternates:400,800');
	
	//OWL Carousel
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/owl-carousel/assets/owl.carousel.min.css', array(), '2.3.4');
	wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/owl-carousel/owl.carousel.min.js', array('jquery'), '2.3.4', true );

	wp_enqueue_script( 'mohole-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'mohole-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	wp_enqueue_script( 'init', get_template_directory_uri() . '/js/init.js', array('jquery', 'owl-carousel'), '1.0.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'mohole_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Sezione widget
 */
require get_template_directory() . '/inc/widgets.php';

/**
 * Sezione tools
 */
require get_template_directory() . '/inc/tools.php';

/**
 * Sezione custom post / custom taxonomy
 */
require get_template_directory() . '/inc/cpt.php';
