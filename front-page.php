<?php
/**
 * 
 *
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mohole
 */

get_header();
?>

<section class="technology">
    <h2>Technology</h2>
    <ul>
    <li>
        <i class="fa fa-calendar"></i>
        <h3 class="technology__title">Schedule on the fly</h3>
        <p>
        Duis pulvinar arcu a ultrices monk dapibus. Etiam suscipit sed quam vel auctor.
        </p>
    </li>
    <li>
        <i class="fa fa-commenting-o"></i>
        <h3>Take notes and reminders</h3>
        <p>
        consectetur adipiscing elit. Sed neque mauris, porta id arcu ac, sagittis auctor ante.
        </p>
    </li>
    <li>
        <i class="fa fa-dropbox"></i>
        <h3>Save everything to Dropbox</h3>
        <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit necessitatibus.
        </p>
    </li>
    <li>
        <i class="fa fa-envelope-o"></i>
        <h3>Take control over messages</h3>
        <p>
        Blandit porta, viverra nec metus. Maecenas in magna ullamcorper dolor commodo.
        </p>
    </li>
    </ul>
</section><!-- technology -->

<section class="experts">
    <h2>Our experts</h2>
    <div class="experts__wrapper">
        <?php
        $args = array(
            'nopaging' => 'true',
            'posts_per_page' => -1,
            'post_type' => 'esperti'
        );
        $query = new WP_query($args);
        if ($query->have_posts()) {
            while ($query->have_posts()) {
                $query->the_post();
                $dummy = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()),'full');
                $src = $dummy[0];
                ?>
                <div class="experts__item">
                    <img class="experts__photo" src="<?php echo $src ?>">
                    <p class="experts__bio">
                        <?php the_excerpt() ?>
                    </p>
                    <h3 class="experts__name">
                        <a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                    </h3>
                </div>
        <?php
            }
        }    
        wp_reset_query();
        wp_reset_postdata();
        ?>
    </div>
    <h3><a href="/wppro_6/esperti/">Show all the experts</a></h3>
</section><!-- experts-->

<section class="testimonials">
    <div class="testimonials__overlay">
        <i class="fa fa-diamond"></i>
        <h3>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsa ipsum suscipit impedit at, commodi magnam pariatur doloremque assumenda saepe in dolorem facere labore minima quis. Velit corrupti architecto quasi qui.
        </h3>
    </div>
</section><!-- testimonials-->

<section class="clients">
    <h2>Our clients</h2>
    <div class="clients__wrapper">
        <ul>
            <li><img src="https://library.elementor.com/wp-content/uploads/2016/08/snycy.png"></li>
            <li><img src="https://library.elementor.com/wp-content/uploads/2016/08/x.venox_.png"></li>
            <li><img src="https://library.elementor.com/wp-content/uploads/2016/08/quest.png"></li>
            <li><img src="https://library.elementor.com/wp-content/uploads/2016/08/geo.png"></li>
            <li><img src="https://library.elementor.com/wp-content/uploads/2016/08/madrin.png"></li>
            <li><img src="https://library.elementor.com/wp-content/uploads/2016/08/digit.png"></li>
            <li><img src="https://library.elementor.com/wp-content/uploads/2016/08/mapmaster.png"></li>
            <li><img src="https://library.elementor.com/wp-content/uploads/2016/08/masso.png"></li>
        </ul>
    </div>
</section><!-- clients -->
<?php get_footer(); ?>
</body>
</html>