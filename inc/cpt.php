<?php

/**
 * Custom posts / custom taxonomies
 */

if (!function_exists('mohole_add_cpt_progetti')) {
	function mohole_add_cpt_progetti() {
	 	$args = array(
            'label'                 => __( 'Progetti', 'mohole' ),
	 		'supports'              => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes' ),
	 		'taxonomies'            => array( 'category', 'post_tag' ),
	 		'hierarchical'          => false,
	 		'public'                => true,
	 		'show_ui'               => true,
	 		'show_in_menu'          => true,
	 		'menu_position'         => 26,
	 		'show_in_admin_bar'     => true,
	 		'show_in_nav_menus'     => true,
	 		'can_export'            => true,
	 		'has_archive'           => true,
	 		'exclude_from_search'   => false,
	 		'publicly_queryable'    => true,
	 		'capability_type'       => 'page',
             'menu_icon'            => 'dashicons-format-gallery'
	 	);
 		register_post_type( 'progetti', $args );
 	}
	add_action( 'init', 'mohole_add_cpt_progetti', 0 );
}

if (!function_exists( 'mohole_add_custom_tax_anni')) {
	function mohole_add_custom_tax_anni() {
		$args = array(
            'label'                 => __( 'Anni', 'mohole' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'show_in_nav_menus'     => true,
			'show_tagcloud'         => true,
		);
		register_taxonomy('anni', array('progetti'), $args );
	}
	add_action( 'init', 'mohole_add_custom_tax_anni', 0 );
}
?>