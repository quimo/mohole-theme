<?php

/**
 * Recupero e mostro i menu
 */
function mohole_show_menu($location, $class = '') {
    if (!$location) return;
    $menu = wp_nav_menu(array(
        'theme_location' => $location,
        'item_spacing' => 'discard',
        'menu_class' => "menu $class",
        'echo' => false,
        'container' => 'ul',
    ));
    if ($menu) return $menu;
}

/**
 * Mostro la featured image o lo slider
 */
function mohole_get_hero_image_or_slider() {
    if (is_home() && get_option('page_for_posts') ) {
        //sono nella home del blog
        $img = wp_get_attachment_image_src(get_post_thumbnail_id(get_option('page_for_posts')),'mohole_hero_image'); 
        $featured_image = $img[0];
        $html = '<div class="post-thumbnail">';
        $html .= '<img src="' . $featured_image . '">';
        $html .= "</div>";
        echo $html;
    } else {
        if (function_exists('get_field') && get_field('mohole_slide1')) {
            $html = '<div class="owl-carousel">';
                //recupero la featured image
                $html .= (get_the_post_thumbnail()) ? '<div>' . get_the_post_thumbnail(get_the_ID(), 'mohole_hero_image') . '</div>' : '';
                $html .= '<div><img src="' . mohole_get_acf_hero_image('mohole_slide1') . '" alt=""></div>';
                $html .= (mohole_get_acf_hero_image('mohole_slide2')) ? '<div><img src="' . mohole_get_acf_hero_image('mohole_slide2') . '" alt=""></div>' : '';
            $html .= '</div>';
            echo $html;
        } else {
            mohole_post_thumbnail('mohole_hero_image');
        }
    }
}
function mohole_get_acf_hero_image($field) {
    $dummy = get_field($field);
    if ($dummy) return $dummy['sizes']['mohole_hero_image'];
    return false;
}