<?php

/**
 * Registro le sidebar per header e footer
 */

add_action('widgets_init','mohole_register_headerfooter');
function mohole_register_headerfooter() {
	// header #1
	register_sidebar(array(
		'name'          => 'Header 1',
		'id'            => 'sidebar_header-1',
		'before_widget' => '<div id="%1$s" class="widget widget--header-1 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
	// header #2
	register_sidebar(array(
		'name'          => 'Header 2',
		'id'            => 'sidebar_header-2',
		'before_widget' => '<div id="%1$s" class="widget widget--header-2 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
	// footer #1
	register_sidebar(array(
		'name'          => 'Footer 1',
		'id'            => 'sidebar_footer-1',
		'before_widget' => '<div id="%1$s" class="widget widget--footer-1 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
	// footer #2
	register_sidebar(array(
		'name'          => 'Footer 2',
		'id'            => 'sidebar_footer-2',
		'before_widget' => '<div id="%1$s" class="widget windget--footer-2 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
	// footer #3
	register_sidebar(array(
		'name'          => 'Footer 3',
		'id'            => 'sidebar_footer-3',
		'before_widget' => '<div id="%1$s" class="widget widget--footer-3 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
	// footer #4
	register_sidebar(array(
		'name'          => 'Footer 4',
		'id'            => 'sidebar_footer-4',
		'before_widget' => '<div id="%1$s" class="widget widget--footer-4 %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}

/**
 * registro le sidebar custom
 */

add_action('widgets_init','mohole_register_sidebar');
function mohole_register_sidebar() {
	// #contatti
	register_sidebar(array(
		'name'          => 'Contatti',
		'id'            => 'contatti',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>'
	));
}

/**
 * recupero la sidebar
 */

function mohole_get_sidebar($sidebar = 'sidebar-1') {
	global $post;
	/* ritorna il nome del file del template solo se non uso il 'template standard' */
	$slug = get_page_template_slug($post->ID);
	if ($slug) list($filename, $extension) = explode('.', $slug);
	else $filename = '';
    /* se non ho una sidebar con il nome uguale al nome del file di un template (estensione esclusa) */
    if (!dynamic_sidebar($filename)) {
        $custom_post_type = get_post_type($post->ID);
		/* se non ho una sidebar con il nome uguale allo slug di un custom post */
        if (!dynamic_sidebar($custom_post_type)) {
            /* se non ho una sidebar con il nome della pagina */
            if (!dynamic_sidebar($post->post_name)) {
                if (is_active_sidebar($sidebar)) {
                    dynamic_sidebar($sidebar);
                }
            }
        }
    }
}

/**
 * recupero le colonne di header e footer
 */
function mohole_get_template_items($sidebar) {
	global $wp_registered_sidebars;
	$html = '';
	$header_content = false;
	$footer_content = false;
	foreach ($wp_registered_sidebars as $key => $value) {
		$dummy = explode('-', $key);
		if ($dummy && !empty($dummy)) {
			$sidebar_type = $dummy[0];
			switch ($sidebar_type) {
				case 'sidebar_footer':
					if ($sidebar == 'sidebar_footer') {
						ob_start();
						$bool = dynamic_sidebar( $key );
						$content = ob_get_contents();
						ob_end_clean();
						if ($bool === true) {
							$html .= "<div class=\"site-footer__col\">";
							$html .= __($content, 'moho');
							$html .= "</div>";
							$footer_content = true;
						}
					}
					break;
				case 'sidebar_header':
					if ($sidebar == 'sidebar_header') {
						ob_start();
						$bool = dynamic_sidebar( $key );
						$content = ob_get_contents();
						ob_end_clean();
						if ($bool === true) {
							$html .= "<div class=\"topbar__item\">";
							$html .= __($content, 'moho');
							$html .= "</div>";
							$header_content = true;
						}
					}
					break;
			}
		}
	}
	if (($sidebar == 'sidebar_header' && $header_content) || ($sidebar == 'sidebar_footer' && $footer_content)) return $html;
	return false;
}