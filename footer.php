<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Mohole
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">

		<div class="mohole-wrapper mohole-wrapper--flex">
			<div class="site-footer__cols">
				<?php echo mohole_get_template_items('sidebar_footer'); ?>
			</div>
		</div><!--wrapper-->

		<div class="site-info">
			<div class="mohole-wrapper">
				<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'mohole' ) ); ?>">
					<?php
					/* translators: %s: CMS name, i.e. WordPress. */
					printf( esc_html__( 'Proudly powered by %s', 'mohole' ), 'WordPress' );
					?>
				</a>
				<span class="sep"> | </span>
					<?php
					/* translators: 1: Theme name, 2: Theme author. */
					printf( esc_html__( 'Theme: %1$s', 'mohole' ), 'mohole' );
					?>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
