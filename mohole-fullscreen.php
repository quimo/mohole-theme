<?php
/**
 * 
 * Template Name: Mohole Fullwidth
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mohole
 */

get_header();
?><div id="content" class="site-content"><!-- compatibilità con Elementor --><?php
    while (have_posts()) {
        the_post();
        get_template_part( 'template-parts/content', 'page' );
        if (comments_open() || get_comments_number()) comments_template();
    }
    get_footer();
