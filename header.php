<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Mohole
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge, ">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mohole' ); ?></a>

	<header id="masthead" class="site-header">
		<nav id="site-navigation" class="main-navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'mohole' ); ?></button>
			<?php echo mohole_show_menu('top_menu', 'menu--top'); ?>
			<?php echo mohole_show_menu('main_menu', 'menu--main'); ?>
		</nav><!-- #site-navigation -->
		<?php if (!is_front_page()): ?>
			<?php //mohole_post_thumbnail('mohole_hero_image'); ?>
			<?php if (get_post_type() != 'esperti') mohole_get_hero_image_or_slider() ?>
		<?php else: ?>	
		<div class="hero">
			<div class="hero__overlay">
				<h1 class="hero__title"><span>Tasker</span><br>Manage all Your Daily Tasks Through A Single App</h1>
				<img class="hero__image" src="https://library.elementor.com/wp-content/uploads/2016/08/logo-2.png">
				<div class="hero__buttons">
					<a class="hero__button hero__button--secondary" href="#">Video tour</a>
					<a class="hero__button hero__button--primary" href="#">Download the app</a>
				</div>
			</div>
		</div>		
		<?php endif; ?>	
	</header><!-- #masthead -->
