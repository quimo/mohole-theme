<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Mohole
 */

get_header();
?>
<div id="content" class="site-content"><!-- compatibilità con Elementor -->
    <div class="mohole-wrapper mohole-wrapper--flex">
		<div id="primary" class="content-area">
			<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', get_post_type() );

				the_post_navigation();

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>

			</main><!-- #main -->
			</div><!-- #primary -->
			<aside id="secondary" class="widget-area">
				<?php mohole_get_hero_image_or_slider() ?>
			</aside><!-- #secondary -->
	</div><!--mohole-wrapper-->
	<?php get_footer();
