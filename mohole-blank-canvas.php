<?php
/**
 * 
 * Template Name: Mohole Blank Canvas
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package mohole
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <?php wp_head();?>
</head>
<body <?php body_class(); ?>>
    <?php mohole_get_hero_image_or_slider() ?>
    <?php
    while (have_posts()) {
        the_post();
        get_template_part( 'template-parts/content', 'page' );
        if (comments_open() || get_comments_number()) comments_template();
    }
    wp_footer();
    ?>
</body>
</html>