<?php get_header(); ?>
<div id="content" class="site-content"><!-- compatibilità con Elementor -->
    <div class="mohole-wrapper mohole-wrapper--flex">
        <div id="primary" class="content-area">
            <main id="main" class="site-main">
            <?php
            while (have_posts()) {
                the_post();
                get_template_part( 'template-parts/content', 'page' );
                if (comments_open() || get_comments_number()) comments_template();
            }
            ?>
            </main>
        </div><!-- #primary -->
        <?php get_sidebar(); ?>
    </div><!--mohole-wrapper-->
    <?php get_footer();